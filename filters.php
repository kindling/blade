<?php
/**
 * Kindling Blade - Filters.
 *
 * @package Kindling_Blade
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

if (!function_exists('add_action')) {
    return;
}

add_action('kindling_ready', function () {
    /**
     * Template Hierarchy should search for .blade.php files
     */
    collect([
        'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
        'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
    ])->map(function ($type) {
        add_filter("{$type}_template_hierarchy", function ($templates) {
            return collect($templates)->flatMap(function ($template) {
                $transforms = [
                    '%^/?(resources[\\/]views)?[\\/]?%' => '',
                    '%(\.blade)?(\.php)?$%' => ''
                ];
                $normalizedTemplate = preg_replace(array_keys($transforms), array_values($transforms), $template);

                return ["{$normalizedTemplate}.blade.php", "{$normalizedTemplate}.php"];
            })->toArray();
        });
    });

    /**
     * Render page using Blade
     */
    add_filter('template_include', function ($template) {
        // Parse WooCommerce templates via kindling/wp-content/themes/base/woocommerce-loader.blade.php.
        if (strpos($template, 'woocommerce')) {
            $wc_template = $template;
            $template = 'woocommerce-loader';
        }

        // Return TranslatePress templates without blade loader.
        if (strpos($template, 'translation-manager.php') || strpos($template, 'string-translation-editor.php')) {
            return $template;
        }

        // Clean up the view name.
        $view = trim(str_replace(array_merge(
            kindling_blade_view_paths(),
            ['.blade.php', '.php']
        ), '', $template), '/');

        $data = collect(get_body_class())->reduce(function ($data, $class) use ($view) {
            return apply_filters("kindling/template/{$class}/data", $data, $view);
        }, []);

        // Pass along the WooCommerce template to be included if neeeded.
        if (isset($wc_template)) {
            $data['wc_template'] = $wc_template;
        }

        // Load the view.
        echo view($view, $data);

        // Return a blank file to make WordPress happy
        return get_theme_file_path('index.php');
    }, PHP_INT_MAX);
});
