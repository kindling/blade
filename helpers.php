<?php
/**
 * Kindling Blade - Helpers
 *
 * @package Kindling_Blade
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

/**
 * Retrieves a compiled view.
 *
 * @param string $view
 * @param array $data
 * @param array $mergeData
 * @return string
 */
function view($view, $data = [], $mergeData = [])
{
    return kindling_blade_view_factory()->make($view, $data, $mergeData)->render();
}

/**
 * Gets the view paths
 *
 * @return array
 */
function kindling_blade_view_paths()
{
    return array_unique(apply_filters('kindling_blade_view_paths', [
        get_stylesheet_directory(),
        get_stylesheet_directory() . '/views',
        get_template_directory(),
        get_template_directory() . '/views',
    ]));
}

/**
 * Gets the view factory
 *
 * @return Illuminate\View\Factory;
 */
function kindling_blade_view_factory()
{
    static $viewFactory;

    if (!isset($viewFactory)) {
        $viewFactory = (new \Kindling\Blade\ViewFactory)->setup();
    }

    return $viewFactory;
}

 /**
  * Gets the compiled view path.
  *
  * @return string
  */
function kindling_blade_compiled_path()
{
    return apply_filters(
        'kindling_blade_compiled_path',
        wp_upload_dir()['basedir'] . '/kindling/blade-compiled'
    );
}
