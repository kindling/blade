<?php

namespace Kindling\Blade;

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Engines\PhpEngine;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;

class ViewFactory
{
    /**
     * Setup the view factory
     *
     * @return Factory
     */
    public function setup()
    {
        // Make sure there is a compiled path
        $this->makeCompiledPath();

        // Dependencies
        $filesystem = new Filesystem;
        $eventDispatcher = new Dispatcher(new Container);

        // Create View Factory capable of rendering PHP and Blade templates
        $viewResolver = new EngineResolver;
        $bladeCompiler = new BladeCompiler($filesystem, kindling_blade_compiled_path());

        $viewResolver->register('blade', function () use ($bladeCompiler, $filesystem) {
            return new CompilerEngine($bladeCompiler, $filesystem);
        });

        $viewResolver->register('php', function () {
            return new PhpEngine;
        });

        $viewFinder = new FileViewFinder($filesystem, kindling_blade_view_paths());

        return new Factory($viewResolver, $viewFinder, $eventDispatcher);
    }

    /**
     * Makes the compiled directory if it does not already exist.
     *
     * @return void
     */
    public function makeCompiledPath()
    {
        $path = rtrim(kindling_blade_compiled_path(), '/');
        if (file_exists($path)) {
            return;
        }

        // Make the directory and add a gitignore
        mkdir(kindling_blade_compiled_path(), 0755, $recursive = true);
        file_put_contents($ignore = kindling_blade_compiled_path() . '/.gitignore', "*\n!.gitignore\n!index.php");
        chmod($ignore, 0755);
        file_put_contents($index = kindling_blade_compiled_path() . '/index.php', "<?php\n// Silence is golden.");
        chmod($index, 0755);
    }
}
